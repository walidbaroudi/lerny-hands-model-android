package com.comp491.lernyhandsmodel

fun FloatArray.indexOfMax() = maxOrNull()?.let { max -> indexOfFirst { it == max } }