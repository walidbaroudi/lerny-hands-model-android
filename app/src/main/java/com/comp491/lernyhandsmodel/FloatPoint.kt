package com.comp491.lernyhandsmodel

data class FloatPoint(val x: Float, val y: Float)
