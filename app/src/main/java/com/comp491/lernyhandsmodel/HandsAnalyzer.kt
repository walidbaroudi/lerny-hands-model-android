package com.comp491.lernyhandsmodel

import android.annotation.SuppressLint
import android.content.res.Configuration
import android.graphics.*
import android.media.Image
import android.util.Log
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import java.io.ByteArrayOutputStream
import android.graphics.Bitmap


class HandsAnalyzer(private val intervalFrame: Int, private val orientation: Int, private val bitmapHandler: (Bitmap) -> Unit) : ImageAnalysis.Analyzer {

    private var counter = 0

    @SuppressLint("UnsafeOptInUsageError")
    override fun analyze(image: ImageProxy) {

        image.image?.toBitmap()?.let { bitmapImage ->
            if (counter % intervalFrame == 0) {
                val correctedBitmap = getCorrectedBitmap(bitmapImage)
                bitmapHandler.invoke(correctedBitmap)
            }
        } ?: run { Log.e("TAG", "handleBitmap: NULL BITMAP") }
        counter++
        image.close()
    }

    private fun getCorrectedBitmap(bitmap: Bitmap): Bitmap {
        return if (orientation == Configuration.ORIENTATION_PORTRAIT)
            bitmap.rotate(-90F).flipHorizontal()
        else
            bitmap.flipHorizontal()
    }

    private fun Image.toBitmap(): Bitmap {
        val yBuffer = planes[0].buffer // Y
        val vuBuffer = planes[2].buffer // VU

        val ySize = yBuffer.remaining()
        val vuSize = vuBuffer.remaining()

        val nv21 = ByteArray(ySize + vuSize)

        yBuffer.get(nv21, 0, ySize)
        vuBuffer.get(nv21, ySize, vuSize)

        val yuvImage = YuvImage(nv21, ImageFormat.NV21, this.width, this.height, null)
        val out = ByteArrayOutputStream()
        yuvImage.compressToJpeg(Rect(0, 0, yuvImage.width, yuvImage.height), 50, out)
        val imageBytes = out.toByteArray()
        return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
    }

}