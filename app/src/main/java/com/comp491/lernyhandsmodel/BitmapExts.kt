package com.comp491.lernyhandsmodel

import android.graphics.Bitmap
import android.graphics.Matrix

fun Bitmap.rotate(degrees: Float): Bitmap {
    val matrix = Matrix().apply { postRotate(degrees) }
    val scaledBitmap = Bitmap.createScaledBitmap(this, width, height, true)
    return Bitmap.createBitmap(scaledBitmap, 0, 0, width, height, matrix, true)
}

fun Bitmap.flipHorizontal(): Bitmap {
    return Bitmap.createScaledBitmap(this, width * -1, height, true)
}

fun Bitmap.flipVertical(): Bitmap {
    return Bitmap.createScaledBitmap(this, width, height * -1, true)
}