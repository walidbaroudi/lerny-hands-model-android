package com.comp491.lernyhandsmodel

import android.Manifest
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.comp491.lernyhandsmodel.databinding.ActivityMainBinding
import com.google.mediapipe.solutions.hands.Hands
import com.google.mediapipe.solutions.hands.HandsOptions
import com.google.mediapipe.solutions.hands.HandsResult
import java.lang.Exception
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import com.comp491.lernyhandsmodel.ml.HandGestureModel
import org.tensorflow.lite.DataType
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer
import java.nio.ByteBuffer


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var cameraExecutor: ExecutorService

    private lateinit var handsModel: Hands
    private lateinit var gestureModel: HandGestureModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initGestureModel()
        initHandsApi()

        // Request camera permissions
        if (allPermissionsGranted()) {
            startCamera()
        } else {
            ActivityCompat.requestPermissions(
                this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS
            )
        }

        cameraExecutor = Executors.newSingleThreadExecutor()
    }

    private fun initGestureModel() {
        gestureModel = HandGestureModel.newInstance(this)
    }

    private fun initHandsApi() {
        val handsOptions = HandsOptions.builder()
            .setStaticImageMode(true)
            .setMaxNumHands(1)
            .setRunOnGpu(true)
            .build()

        handsModel = Hands(this, handsOptions)

        handsModel.setResultListener { handleHandsResult(it) }

    }

    private fun startCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        cameraProviderFuture.addListener({
            val provider = cameraProviderFuture.get()
            val preview = Preview.Builder().build().also { it.setSurfaceProvider(binding.viewCamera.surfaceProvider) }
            val selector = CameraSelector.DEFAULT_FRONT_CAMERA

            // register hands analyzer
            val orientation = resources.configuration.orientation
            val analyzer = HandsAnalyzer(1, orientation) { handleBitmap(it) }
            val imageAnalyzer = ImageAnalysis.Builder().build().also { it.setAnalyzer(cameraExecutor, analyzer) }
            try {
                provider.unbindAll()
                provider.bindToLifecycle(this, selector, preview, imageAnalyzer)
            } catch (e: Exception) {
                Toast.makeText(this, "Failed to create preview", Toast.LENGTH_SHORT).show()
            }
        }, ContextCompat.getMainExecutor(this))
    }

    private fun handleBitmap(bitmap: Bitmap) {
        handsModel.send(bitmap)
        runOnUiThread { binding.imgPreview.setImageBitmap(bitmap) }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        startCamera()
    }

    private fun handleHandsResult(result: HandsResult) {
        val coordinates = result.get2DCoordinates()
        binding.viewQuarters.setCurrentPoint(coordinates)
        coordinates?.let {
            Log.i(TAG, "INDEX FINGER AT: X= ${it.x}, Y=${it.y}")
        }

        val frame = result.inputBitmap()
        val landmarksBuffer = result.getLandmarksByteBuffer(frame.width, frame.height)
        sendLandmarksToGestureModel(landmarksBuffer)
    }

    private fun sendLandmarksToGestureModel(landmarksBuffer: ByteBuffer?) {
        // Creates inputs for reference.
        landmarksBuffer?.let { buffer ->
            val inputFeature0 = TensorBuffer.createFixedSize(intArrayOf(1, 21, 2), DataType.FLOAT32)
            inputFeature0.loadBuffer(buffer)

            // Runs model inference and gets result.
            val outputs = gestureModel.process(inputFeature0)
            val outputFeature = outputs.outputFeature0AsTensorBuffer
            val probabilities = outputFeature.floatArray
            probabilities.indexOfMax()?.let { index ->
                val gesture = Gesture.values()[index]
                setGestureLabel(gesture)
            }
            Log.i(TAG, "Gesture Model: ${probabilities.contentToString()}")
        } ?: setGestureLabel(null)
    }

    private fun setGestureLabel(gesture: Gesture?) {
        runOnUiThread { binding.tvGesture.text = gesture?.stringValue ?: "" }
    }

    override fun onDestroy() {
        super.onDestroy()
        gestureModel.close()
        cameraExecutor.shutdown()
    }

    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
            baseContext, it
        ) == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults:
        IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                startCamera()
            } else {
                Toast.makeText(
                    this,
                    "Permissions not granted by the user.",
                    Toast.LENGTH_SHORT
                ).show()
                finish()
            }
        }
    }

    companion object {
        private const val TAG = "CameraXApp"
        private const val FILENAME_FORMAT = "yyyy-MM-dd-HH-mm-ss-SSS"
        private const val REQUEST_CODE_PERMISSIONS = 10
        private val REQUIRED_PERMISSIONS =
            mutableListOf(
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO
            ).toTypedArray()
    }
}