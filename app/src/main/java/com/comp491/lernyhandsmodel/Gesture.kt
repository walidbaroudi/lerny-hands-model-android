package com.comp491.lernyhandsmodel

enum class Gesture(val stringValue: String) {
    OKAY("Okay"),
    PEACE("Peace"),
    THUMBS_UP("Thumbs up"),
    THUMBS_DOWN("Thumbs down"),
    CALL_ME("Call me"),
    STOP("Stop"),
    ROCK("Rock"),
    LIVE_LONG("Live long"),
    FIST("Fist"),
    SMILE("Smile")
}