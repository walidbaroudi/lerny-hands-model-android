package com.comp491.lernyhandsmodel

import android.content.Context
import android.content.res.Resources
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.comp491.lernyhandsmodel.databinding.LayoutQuartersViewBinding
import android.util.DisplayMetrics


class QuartersView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : FrameLayout(context, attrs, defStyle) {

    private val binding = LayoutQuartersViewBinding.inflate(LayoutInflater.from(context), this, true)

    private val quarters = mutableListOf<View>()

    init {
        with(binding) {
            quarters.add(quarters.size, viewTopRight)
            quarters.add(quarters.size, viewTopLeft)
            quarters.add(quarters.size, viewBotLeft)
            quarters.add(quarters.size, viewBotRight)
        }
        deselectAll()
    }

    private fun deselectAll() {
        quarters.forEach { it.isSelected = false }
    }

    fun setCurrentPoint(point: FloatPoint?) {
        deselectAll()
        point ?: return
        getQuarter(point.x, point.y)?.let { quarter ->
            quarters[quarter].isSelected = true
        }
    }

    private fun getQuarter(x: Float, y: Float): Int? {
        val firstHalf = 0.0..0.5
        val secondHalf = 0.5..1.0
        return when {
            (x in secondHalf) && (y in firstHalf) -> 0 // first quarter
            x in firstHalf && y in firstHalf -> 1 // second quarter
            x in firstHalf && y in secondHalf -> 2 // third quarter
            x in secondHalf && y in secondHalf -> 3 // fourth quarter
            else -> null
        }
    }
}